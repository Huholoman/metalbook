<?php
/**
 * Created by PhpStorm.
 * User: tobik
 * Date: 24.3.14
 * Time: 22:38
 */

namespace App\Models\Repository;


class UserRepository extends BaseRepository
{

    /**
     * Vrátí uživatele podle jmena.
     *
     * @param $name
     * @return mixed
     * @throws Exception
     */
    public function findByLogin($name)
    {

        $row = $this->connection->select("*")
            ->from($this->getTable())
            ->where("login = %s", $name)
            ->fetch();

        if ($row === false) {
            throw new \Nette\Security\AuthenticationException('The login is incorrect.', self::IDENTITY_NOT_FOUND);
        }

        return $this->createEntity($row);
    }
} 