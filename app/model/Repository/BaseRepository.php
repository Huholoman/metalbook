<?php
/**
 * Created by PhpStorm.
 * User: tobik
 * Date: 24.3.14
 * Time: 20:10
 */

namespace App\Models\Repository;


use LeanMapper\Repository;
use Nette\Diagnostics\Debugger;

class BaseRepository extends Repository
{

    public function find($id)
    {
        $row = $this->connection->select('*')
            ->from($this->getTable())
            ->where('id = %iN', $id)
            ->fetch();

        if ($row === false) {
            return null;
        }
        return $this->createEntity($row);
    }

    public function findBy(array $by)
    {
        $fluent = $this->connection->select('*')
            ->from($this->getTable());

        foreach ($by as $key => $value) {
            if ($value) {
                $fluent->where("%n = %s", $key, $value);
            } else {
                $fluent->where("%n IS NULL", $key);
            }
        }

        $row = $fluent->fetch();
        Debugger::dump($fluent->test());
        if ($row === false) {
            return null;
        }
        return $this->createEntity($row);
    }

    public function findAll()
    {
        return $this->createEntities(
            $this->connection->select('*')
                ->from($this->getTable())
                ->fetchAll()
        );
    }

    public function findAllBy(array $by)
    {

        $fluent = $this->connection->select('*')
            ->from($this->getTable());

        foreach ($by as $key => $value) {
            if ($value) {
                $fluent->where("%n = %i", $key, $value);
            } else {
                $fluent->where("%n IS NULL", $key);
            }
        }

        return $this->createEntities(
            $fluent->fetchAll()
        );
    }
} 