<?php
/**
 * Created by PhpStorm.
 * User: tobik
 * Date: 24.3.14
 * Time: 20:09
 */

namespace App\Models\Repository;


use Nette\Diagnostics\Debugger;

class NavMenuItemRepository extends BaseRepository
{


    /**
     * Vrátí entity zgrupované podle id kategorie.
     *
     * @param array $typeIds Id typu položek menu
     * @return array
     */
    public function findByTypesGroupedByCategory($typeIds = array())
    {

        return $this->createEntities(
            $this->connection->select("*")
                ->from($this->getTable())
                ->where("nav_menu_type_id IN %in", $typeIds)
                ->groupBy("nav_category_id")
                ->fetchAll()
        );
    }

    /**
     * Vrátí entitu podle linku.
     *
     * @param $link
     * @throws Exception
     * @return MenuItem
     */
    public function findIdByLink($link)
    {

        $row = $this->connection->select("*")
            ->from($this->getTable())
            ->where("link = %s", $link)
            ->fetch();

        if (!$row) {
            return null;
        }

        return $this->createEntity($row);
    }

    /**
     * Vrátí menu itemy podle kategorie a menu typů.
     *
     * @param $categoryId
     * @param array $typeIds
     * @return array
     */
    public function findByCategoryIdAndMenuTypeIds($categoryId, $typeIds = array())
    {

        return $this->createEntities(
            $this->connection->select("*")
                ->from($this->getTable())
                ->where("nav_category_id = %s", $categoryId)
                ->where("nav_menu_type_id IN %in", $typeIds)
                ->fetchAll()
        );
    }
}