<?php
/**
 * Created by PhpStorm.
 * User: tobik
 * Date: 3.3.14
 * Time: 22:51
 */

namespace App\Models\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @author tobik
 * @property int $id
 * @property string $link
 * @property string $defaultLink
 * @property string $name
 * @property int $order
 * @property NavMenuItem[] $menuItems m:belongsToMany(nav_category_id:nav_menu_item)
 *
 */
class NavCategory extends \LeanMapper\Entity
{

} 