<?php
/**
 * Created by PhpStorm.
 * User: tobik
 * Date: 5.3.14
 * Time: 21:50
 */

namespace App\Models\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * @author tobik
 * @property int $id
 * @property string $name
 * @property string $description
 * @property ForumTopic[] $children m:belongsToMany(id:parent_id)
 * @property ForumTopic $parent m:hasOne(parent_id)
 * @property int $stickedTop
 */
class ForumTopic extends \LeanMapper\Entity
{

} 