<?php
/**
 * Created by PhpStorm.
 * User: tobik
 * Date: 26.2.14
 * Time: 22:16
 */

namespace App\Models\Entity;

/**
 * @author tobik
 *
 * @property int $id
 * @property string $name
 * @property string $link
 * @property NavCategory $navCategory m:hasOne(nav_category_id:nav_category)
 * @property int $order
 * @property NavMenuType $menuType m:belongsToOne
 */
class NavMenuItem extends \LeanMapper\Entity
{

} 