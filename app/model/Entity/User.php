<?php
/**
 * Created by PhpStorm.
 * User: tobik
 * Date: 25.2.14
 * Time: 20:37
 */

namespace App\Models\Entity;

use Doctrine\ORM\Mapping as ORM;
use Model\Authorizator;

/**
 * @author tobik
 * @property int $id
 * @property string $login
 * @property string $password
 * @property string $role
 * @property string $email
 * @property int $birthday
 * @property string $description
 * @property string $musicDescription
 */
class User extends \LeanMapper\Entity
{

}