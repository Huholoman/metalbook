<?php
/**
 * Created by PhpStorm.
 * User: tobik
 * Date: 26.2.14
 * Time: 19:58
 */

namespace App\Models\Entity;


/**
 * @author tobik
 * @property int $id
 * @property string $name
 */
class Role extends \LeanMapper\Entity
{

    const
        R_VISITOR = "visitor",
        R_ADMIN = "admin";
} 