<?php
/**
 * Created by PhpStorm.
 * User: tobik
 * Date: 23.3.14
 * Time: 18:46
 */

namespace App\Models\Entity;

/**
 * @author tobik
 * @property int $id
 * @property string $const
 * @property MenuItem $menuItems m:hasMany
 */
class NavMenuType extends \LeanMapper\Entity
{

    const
        MENU_TYPE_ALL = 1,
        MENU_TYPE_NLG = 2,
        MENU_TYPE_REG = 3;

} 