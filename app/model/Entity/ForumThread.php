<?php
/**
 * Created by PhpStorm.
 * User: tobik
 * Date: 24.3.14
 * Time: 23:42
 */

namespace App\Models\Entity;

/**
 * @author tobik
 * @property int $id
 * @property string $title
 * @property string $description
 * @property ForumTopic $parent m:hasOne(forum_topic_id:id)
 * @property User $author m:hasOne(author_id)
 * @property ForumPost[] $posts m:belongsToMany
 */
class ForumThread extends \LeanMapper\Entity
{

} 