<?php
/**
 * Created by PhpStorm.
 * User: tobik
 * Date: 24.3.14
 * Time: 23:44
 */

namespace App\Models\Entity;

/**
 * @author tobik
 * @property int $id
 * @property User $author m:hasOne(author_id)
 * @property string $content
 * @property ForumThread $thread m:hasOne(forum_thread_id:id)
 */
class ForumPost extends \LeanMapper\Entity
{

} 