<?php

namespace App\Models;

use App\Models\Repository\UserRepository;
use Nette,
    Nette\Utils\Strings;


/**
 * Users management.
 */
class UserManager extends Nette\Object implements Nette\Security\IAuthenticator
{
    const
        SALT = 'nejakyC00Lvo$oLEni';


    /** @var Nette\Database\Context */
    private $userRepository;


    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }


    /**
     * Performs an authentication.
     *
     * @return Nette\Security\Identity
     * @throws Nette\Security\AuthenticationException
     * @param array $credentials
     */
    public function authenticate(array $credentials)
    {
        list($login, $password) = $credentials;

        $user = $this->userRepository->findByLogin($login);

        if (self::calculateHash($password) !== $user->getPassword()) {
            throw new Nette\Security\AuthenticationException('The password is incorrect.', self::INVALID_CREDENTIAL);

        }

        $arr = array(//			"entity" => $user
        );
        return new Nette\Security\Identity($user->getId(), array("admin"), $arr);
    }

    public static function calculateHash($string)
    {
        return sha1(sprintf("%s%s", $string, self::SALT));
    }

}
