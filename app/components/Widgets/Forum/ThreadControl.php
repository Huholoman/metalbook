<?php
/**
 * Created by PhpStorm.
 * User: Optical
 * Date: 15.2.14
 * Time: 13:29
 */

namespace components\Widgets\Forum;


use Models\ForumModel;
use Nette\Application\UI\Control;
use Nette\Diagnostics\Debugger;

class ThreadWidget extends Control
{
    private $thread;
    private $model;

    function  __construct($name, $threadId, $page, ForumModel $model)
    {
        parent::__construct(null, $name);
        $this->model = $model;
        $this->thread = $this->model->getThread($threadId, $page);
    }

    public function render()
    {
        $template = $this->template;
        $template->setFile(__DIR__ . '/threadwidget.latte');
        $template->thread = $this->thread;

        $template->render();
    }
}