<?php
/**
 * Created by PhpStorm.
 * User: Optical
 * Date: 15.2.14
 * Time: 13:29
 */

namespace components\Widgets\Forum;

use Models\ForumModel;
use Nette\Application\UI\Control;
use Nette\Diagnostics\Debugger;

class GroupWidget extends Control
{

    private $model;

    function  __construct($name, $parent, ForumModel $model)
    {
        parent::__construct($parent, $name);
        $this->model = $model;
    }

    public function render()
    {
        $template = $this->template;
        $template->setFile(__DIR__ . '/groupControl.latte');


        $template->render();
    }
} 