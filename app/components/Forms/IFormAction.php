<?php
namespace App\Components\Forms;

use Nette\Forms\Form;

interface IFormAction
{

    public function execute(Form $form);
}
