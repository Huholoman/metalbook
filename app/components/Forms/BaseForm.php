<?php
/**
 * Created by PhpStorm.
 * User: tobik
 * Date: 27.3.14
 * Time: 19:22
 */

namespace App\Components\Forms;


use Nette\Application\UI\Form;

class BaseForm extends Form
{

    public function __construct($parent, $name)
    {
        parent::__construct($parent, $name);

//        $this->setRenderer(new BootstrapRenderer);
    }
} 