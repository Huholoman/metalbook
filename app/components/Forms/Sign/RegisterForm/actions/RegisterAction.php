<?php

namespace App\Components\Forms\Sign\actions;

use App\Components\Forms\IFormAction;
use App\Components\Forms\Sign\RegisterForm;
use LeanMapper\Entity;
use App\Models\Entity\User;
use App\Models\Repository\UserRepository;
use App\Models\UserManager;
use App\Models\IUserModel;
use App\Models\UserModel;
use Nette\Forms\Form;

class RegisterAction implements IFormAction
{

    /**
     * @var UserRepository $user
     */
    private $userRepository;

    public function __construct(UserRepository $user)
    {
        $this->userRepository = $user;
    }

    public function execute(Form $form)
    {
        $values = $form->getValues();

        if ($this->userRepository->findBy(array("login" => $values[RegisterForm::FIELD_USERNAME])) instanceof Entity) {
            $form->addError('Uživatel existuje.');
            return;
        }

        if ($values[RegisterForm::FIELD_PASSWORD] !== $values[RegisterForm::FIELD_PASSWORD_VERIFY]) {
            $form->addError("Hesla se neshodují");
            return;
        }

        $user = new User();
        $user->login = $values[RegisterForm::FIELD_USERNAME];
        $user->password = UserManager::calculateHash($values[RegisterForm::FIELD_PASSWORD]);
        $this->userRepository->persist($user);
    }
} 