<?php
/**
 * Created by PhpStorm.
 * User: tobik
 * Date: 7.12.13
 * Time: 14:38
 */

namespace App\Components\Forms\Sign;

use App\Components\Forms\BaseForm;
use Nette\Application\UI\Form;

class RegisterForm extends BaseForm
{

    const
        FIELD_HIDDEN_ID = "pid",
        FIELD_USERNAME = "username",
        FIELD_PASSWORD = "passwd",
        FIELD_PASSWORD_VERIFY = "passwd_verify",
        SUBMIT_REGISTER = "submit_register";

    public function __construct($parent, $name)
    {
        parent::__construct($parent, $name);
        $this->addHidden(self::FIELD_HIDDEN_ID);

        $this->addText(self::FIELD_USERNAME, "Login: ")
            ->setRequired("Chybný login");

        $this->addPassword(self::FIELD_PASSWORD, "Heslo: ")
            ->setRequired("Neplatné heslo.")
            ->addCondition(Form::MIN_LENGTH, 5);

        $this->addPassword(self::FIELD_PASSWORD_VERIFY, "Ověření hesla: ")
            ->setRequired("Musíte zadat heslo pro ověření.")
            ->addRule(Form::EQUAL, "Hesla se neshodují.", $this[self::FIELD_PASSWORD]);

        $this->addSubmit(self::SUBMIT_REGISTER);

        $this->addProtection('Vypršel časový limit, odešlete formulář znovu');
    }
} 