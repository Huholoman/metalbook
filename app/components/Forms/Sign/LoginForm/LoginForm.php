<?php

namespace App\Components\Forms\Sign;


use App\Components\Forms\BaseForm;
use Nette\ComponentModel\IContainer;

class LoginForm extends BaseForm
{
    const
        FIELD_USERNAME = "username",
        FIELD_PASSWORD = "password",
        SUBMIT_SAVE = "submit";

    /**
     * Předdefinuje formulář
     *
     * @param IContainer $parent
     * @param string $name
     */
    public function __construct(IContainer $parent, $name)
    {
        parent::__construct($parent, $name);

        $this->addText(self::FIELD_USERNAME, "Login:")
            ->setRequired("Musíte zadat login.");

        $this->addPassword(self::FIELD_PASSWORD, "Heslo:")
            ->setRequired("Je třeba vyplnit heslo:");

        $this->addSubmit(self::SUBMIT_SAVE, "Přihlásit");

        $this->addProtection('Vypršel časový limit, odešlete formulář znovu');
    }
} 