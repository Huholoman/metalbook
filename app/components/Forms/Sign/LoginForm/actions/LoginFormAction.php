<?php

namespace App\Components\Forms\actions;

use App\Components\Forms\IFormAction;
use App\Components\Forms\Sign\LoginForm;
use Nette\Forms\Form;
use Nette\Security\AuthenticationException;
use Nette\Security\User;

class LoginFormAction implements IFormAction
{
    /**
     * @var User $user
     */
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @param Form $form
     */
    public function execute(Form $form)
    {
        $data = $form->getValues();
        try {
            $this->user->login($data[LoginForm::FIELD_USERNAME], $data[LoginForm::FIELD_PASSWORD]);
        } catch (AuthenticationException $e) {
            $form->addError('Neplatné uživatelské jméno nebo heslo.');
        }
    }
} 