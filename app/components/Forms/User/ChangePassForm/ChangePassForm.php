<?php

namespace App\Components\Forms\User;


use App\Components\Forms\BaseForm;
use Kdyby\BootstrapFormRenderer\BootstrapRenderer;
use Nette\Application\UI\Form;
use Nette\ComponentModel\IContainer;

class ChangePassForm extends BaseForm
{
    const
        FIELD_OLD_PASSWORD = "oldpass",
        FIELD_PASSWORD = "password",
        FIELD_PASSWORD_VERIFY = "confPassword",
        SUBMIT_SAVE = "submit";

    /**
     * Vytvoří a vrátí formulář.
     *
     * @param IContainer $parent
     * @param string $name
     * @return Form
     */
    public function __construct(IContainer $parent, $name)
    {
        parent::__construct($parent, $name);

        $this->addPassword(self::FIELD_OLD_PASSWORD, "Současné heslo:")
            ->setRequired("Musíte zadat Současný heslo.");

        $this->addPassword(self::FIELD_PASSWORD, "Nové heslo:")
            ->setRequired("Je třeba vyplnit Nové heslo.");

        $this->addPassword(self::FIELD_PASSWORD_VERIFY, "Nové heslo znovu:")
            ->setRequired("Je třeba vyplnit Nové heslo znovu.")
            ->addRule(Form::EQUAL, "Hesla se neshodují.", $this[self::FIELD_PASSWORD]);

        $this->addSubmit(self::SUBMIT_SAVE, "Změnit heslo");

//		$this->setRenderer(new BootstrapRenderer());
        $this->addProtection("Vypršel časový limit, odešlete formulář znovu");
    }
} 