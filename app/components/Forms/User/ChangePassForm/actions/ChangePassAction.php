<?php

namespace App\Components\Forms\actions;

use App\Components\Forms\IFormAction;
use App\Components\Forms\User\ChangePassForm;
use App\Models\Repository\UserRepository;
use Nette\Forms\Form;
use Nette\Security\User;
use App\Models\UserManager;

class ChangePassAction implements IFormAction
{

    /** @var UserRepository */
    private $userRepository;
    /**
     * @var User
     */
    private $user;

    public function __construct(UserRepository $userService, User $user)
    {
        $this->userRepository = $userService;
        $this->user = $user;
    }

    /**
     * @param Form $form
     */
    public function execute(Form $form)
    {
        $data = $form->getValues();
        $user = $this->userRepository->find($this->user->getIdentity()->getId());

        if ($user->getPassword() !== UserManager::calculateHash($data[ChangePassForm::FIELD_OLD_PASSWORD])) {
            $form->addError("Staré heslo je špatné.");
            return;
        }

        $newPass = UserManager::calculateHash($data[ChangePassForm::FIELD_PASSWORD]);
        $user->setPassword($newPass);
        $this->userRepository->persist($user);
    }
} 