<?php

namespace App\Components\Forms\actions;

use App\Components\Forms\IFormAction;
use App\Components\Forms\User\ChangeEmailForm;
use App\Models\IUserModel;
use Nette\Forms\Form;

class ChangeEmailAction implements IFormAction
{
    /**
     * @var User $user
     */
    private $model;
    private $user;

    public function __construct(IUserModel $model, $user)
    {
        $this->model = $model;
        $this->user = $user;
    }

    /**
     * @param Form $form
     */
    public function execute(Form $form)
    {
        $data = $form->getValues();
        $this->model->changeEmail($data[ChangeEmailForm::FIELD_EMAIL], $this->user->getId());
    }
} 