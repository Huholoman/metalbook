<?php

namespace App\Components\Forms\User;


use App\Components\Forms\BaseForm;
use Kdyby\BootstrapFormRenderer\BootstrapRenderer;
use Nette\Application\UI\Form;
use Nette\ComponentModel\IContainer;

class ChangeEmailForm extends BaseForm
{
    const
        FIELD_EMAIL = "email",
        FIELD_EMAIL_VERIFY = "confEmail",
        SUBMIT_SAVE = "submit";

    /**
     * Vytvoří a vrátí formulář.
     *
     * @param IContainer $parent
     * @param string $name
     * @return Form
     */
    public function __construct(IContainer $parent, $name)
    {
        parent::__construct($parent, $name);

        $this->addText(self::FIELD_EMAIL, "Nový email:")
            ->setRequired("Je třeba vyplnit Nové email.");

        $this->addSubmit(self::SUBMIT_SAVE, "Změnit email");

        $this->setRenderer(new BootstrapRenderer());
        $this->addProtection('Vypršel časový limit, odešlete formulář znovu');
    }
}