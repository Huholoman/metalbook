<?php

namespace App\Components\Forms\User;


use App\Components\Forms\BaseForm;
use Kdyby\BootstrapFormRenderer\BootstrapRenderer;
use Nette\Application\UI\Form;
use Nette\ComponentModel\IContainer;
use App\Models\UserModel;

class ChangeDataForm extends BaseForm
{
    const
        FIELD_AGE = "age",
        FIELD_MDESC = "musicDescription",
        FIELD_DESC = "description",
        SUBMIT_SAVE = "submit";

    /**
     * Vytvoří a vrátí formulář.
     *
     * @param IContainer $parent
     * @param string $name
     * @return Form
     */
    public function __construct(IContainer $parent, $name)
    {
        parent::__construct($parent, $name);

        $this->addText(self::FIELD_AGE, "Rok narození:")
            ->addRule(Form::INTEGER, 'Rok narození musí být číslo');

        $this->addTextArea(self::FIELD_MDESC, "Hudba:")
            ->addRule(Form::MAX_LENGTH, 'Hudba je příliš dlouhá', 65000);

        $this->addTextArea(self::FIELD_DESC, "Další informace:")
            ->addRule(Form::MAX_LENGTH, 'Další informace jsou příliš dlouhé', 65000);

        $this->addSubmit(self::SUBMIT_SAVE, "Změnit údaje");

        $this->addProtection('Vypršel časový limit, odešlete formulář znovu');
    }
} 