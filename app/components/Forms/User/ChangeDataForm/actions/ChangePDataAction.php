<?php

namespace App\Components\Forms\actions;

use App\Components\Forms\IFormAction;
use App\Components\Forms\User\ChangeDataForm;
use App\Models\Repository\UserRepository;
use App\Models\IUserModel;
use Nette\Forms\Form;
use Nette\Security\User;

class ChangePDataAction implements IFormAction
{
    private $userRepository;
    /**
     * @var User
     */
    private $user;

    public function __construct(UserRepository $userRepository, $user)
    {
        $this->userRepository = $userRepository;
        $this->user = $user;
    }

    public function execute(Form $form)
    {
        $data = $form->getValues();

        $user = $this->userRepository->find($this->user->getId());
        if (!$user) {
            $form->addError("Vyskytla se chyba, uživatel nebyl nalezen! O_o");
            return;
        }

        $user->age = $data[ChangeDataForm::FIELD_AGE];
        $user->description = $data[ChangeDataForm::FIELD_DESC];
        $user->musicDescription = $data[ChangeDataForm::FIELD_MDESC];

        $this->userRepository->persist($user);
    }
} 