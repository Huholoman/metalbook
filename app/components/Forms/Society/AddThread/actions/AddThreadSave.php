<?php
/**
 * Created by PhpStorm.
 * User: tobik
 * Date: 25.3.14
 * Time: 22:48
 */

namespace App\Components\Forms\Society\actions;


use App\Components\Forms\IFormAction;
use App\Components\Forms\Society\AddThreadForm;
use App\Models\Entity\ForumThread;
use App\Models\Entity\ForumTopic;
use App\Models\Entity\User;
use App\Models\Repository\ForumThreadRepository;
use App\Models\Repository\ForumTopicRepository;
use Nette\Forms\Form;

class AddThreadSave implements IFormAction
{

    /**
     * @var ForumThreadRepository
     */
    private $threadRepository;

    /**
     * @var User
     */
    private $user;

    public function __construct(ForumThreadRepository $threadRepository, ForumTopicRepository $topicRepository, User $user)
    {
        $this->threadRepository = $threadRepository;
        $this->topicRepository = $topicRepository;
        $this->user = $user;
    }

    public function execute(Form $form)
    {

        $data = $form->getValues();

        $thread = new ForumThread;
        $thread->id = $data[AddThreadForm::FIELD_ID];
        $thread->title = $data[AddThreadForm::FIELD_TITLE];
        $thread->description = $data[AddThreadForm::FIELD_DESC];
        $thread->author = $this->user;

        $parentId = $data[AddThreadForm::FIELD_TOPIC_ID];
        $parent = $this->topicRepository->find($parentId);

        if (!($parent instanceof ForumTopic)) {
            $form->addError("Neexistující topic..");
            return;
        }
        $thread->parent = $parent;

        $this->threadRepository->persist($thread);
    }
} 