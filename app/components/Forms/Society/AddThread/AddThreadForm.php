<?php
/**
 * Created by PhpStorm.
 * User: tobik
 * Date: 25.3.14
 * Time: 21:39
 */

namespace App\Components\Forms\Society;


use App\Components\Forms\BaseForm;

class AddThreadForm extends BaseForm
{

    CONST
        FIELD_ID = "pid",
        FIELD_TITLE = "title",
        FIELD_DESC = "description",
        FIELD_TOPIC_ID = "topic_id",
        FIELD_SUBMIT = "submit";


    /**
     * @author tobik
     * @property int $id
     * @property string $name
     * @property string $description
     * @property ForumTopic[] $children m:belongsToMany(id:parent_id)
     * @property int $stickedTop
     */


    public function __construct($parent, $name)
    {

        parent::__construct($parent, $name);

        $this->addHidden(self::FIELD_ID);
        $this->addHidden(self::FIELD_TOPIC_ID);

        $this->addText(self::FIELD_TITLE, "Název")//			->addRule(Form::MIN_LENGTH, 5)
        ;

        $this->addText(self::FIELD_DESC, "Popis");

        $this->addSubmit(self::FIELD_SUBMIT, "Přidat");
    }
} 