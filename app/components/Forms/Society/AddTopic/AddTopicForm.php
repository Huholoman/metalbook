<?php
/**
 * Created by PhpStorm.
 * User: tobik
 * Date: 25.3.14
 * Time: 21:39
 */

namespace App\Components\Forms\Society;


use App\Components\Forms\BaseForm;

class AddTopicForm extends BaseForm
{

    CONST
        FIELD_ID = "pid",
        FIELD_NAME = "name",
        FIELD_DESC = "description",
        FIELD_PARENT_ID = "parent_id",
        FIELD_SUBMIT = "submit";


    /**
     * @author tobik
     * @property int $id
     * @property string $name
     * @property string $description
     * @property ForumTopic[] $children m:belongsToMany(id:parent_id)
     * @property int $stickedTop
     */


    public function __construct($parent, $name)
    {

        parent::__construct($parent, $name);

        $this->addHidden(self::FIELD_ID);
        $this->addHidden(self::FIELD_PARENT_ID);

        $this->addText(self::FIELD_NAME, "Název")//			->addRule(Form::MIN_LENGTH, 5)
        ;

        $this->addText(self::FIELD_DESC, "Popis");

        $this->addSubmit(self::FIELD_SUBMIT, "Přidat");
    }
} 