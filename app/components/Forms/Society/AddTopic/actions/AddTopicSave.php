<?php
/**
 * Created by PhpStorm.
 * User: tobik
 * Date: 25.3.14
 * Time: 22:48
 */

namespace App\Components\Forms\Society\actions;


use App\Components\Forms\IFormAction;
use App\Components\Forms\Society\AddTopicForm;
use App\Models\Entity\ForumTopic;
use App\Models\Repository\ForumTopicRepository;
use Nette\Forms\Form;

class AddTopicSave implements IFormAction
{

    /**
     * @var ForumTopicRepository
     */
    private $topicRepository;

    public function __construct(ForumTopicRepository $topicRepository)
    {
        $this->topicRepository = $topicRepository;
    }

    public function execute(Form $form)
    {

        $data = $form->getValues();

        $topic = new ForumTopic;
        $topic->id = $data[AddTopicForm::FIELD_ID];
        $topic->name = $data[AddTopicForm::FIELD_NAME];
        $topic->description = $data[AddTopicForm::FIELD_DESC];

        $parentId = $data[AddTopicForm::FIELD_PARENT_ID];
        if ((int)$parentId > 0) {
            $parent = $this->topicRepository->find($parentId);
            if ($parent instanceof ForumTopic) {
                $topic->parent = $parent;
            }
        }

        $this->topicRepository->persist($topic);
    }
} 