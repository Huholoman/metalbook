<?php
/**
 * Created by PhpStorm.
 * User: tobik
 * Date: 23.3.14
 * Time: 22:29
 */

namespace App\Presenters;


class SecurePresenter extends BasePresenter
{
    public function startup()
    {
        parent::startup();
        if (!$this->getUser()->isLoggedIn()) {
            $this->redirect("Sign:login");
        }
    }
} 