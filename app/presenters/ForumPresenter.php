<?php
/**
 * Created by PhpStorm.
 * User: Optical
 * Date: 15.2.14
 * Time: 13:24
 */

namespace App\Presenters;

use App\Components\Forms\Society\actions\AddThreadSave;
use App\Components\Forms\Society\actions\AddTopicSave;
use App\Components\Forms\Society\AddThreadForm;
use App\Components\Forms\Society\AddTopicForm;
use Nette\Forms\Form;

class ForumPresenter extends SecurePresenter
{

    public function startup()
    {
        parent::startup();
    }

    public function renderDefault()
    {

        $topicId = $this->getParameter("id");

        $topics = $this->context->getService("forumTopicRepository")->findAllBy(array("parent_id" => $topicId));
        $topics = count($topics) > 0 ? $topics : false;

        $threads = $this->context->getService("forumThreadRepository")->findAllBy(array("forum_topic_id" => $topicId));
        $threads = count($threads) > 0 ? $threads : false;

        $this->template->topicId = $topicId;
        $this->template->topics = $topics;
        $this->template->threads = $threads;
    }

    public function renderThread($id)
    {
        $thread = $this->context->getService("forumThreadRepository")->find($id);

        $this->template->thread = $thread;
    }

    public function renderAddTopic($topicId)
    {

    }

    public function renderAddThread($topicId)
    {

    }

    public function createComponentAddTopicForm($name)
    {
        $form = new AddTopicForm($this, $name);

        $topicId = $this->getParameter("topicId");
        if ((int)$topicId > 0) {
            $form->setDefaults(
                array(
                    AddTopicForm::FIELD_PARENT_ID => $topicId
                )
            );
        }

        $action = new AddTopicSave($this->context->getService("forumTopicRepository"));
        $form->onSuccess[] = array($action, "execute");
        $form->onSuccess[] = array($this, "addTopicSuccess");

        return $form;
    }

    public function createComponentAddThreadForm($name)
    {
        $form = new AddThreadForm($this, $name);

        $topicId = $this->getParameter("topicId");
        $user = $this->context->getService("userRepository")->find($this->getUser()->id);

        if ((int)$topicId > 0) {
            $form->setDefaults(
                array(
                    AddThreadForm::FIELD_TOPIC_ID => $topicId
                )
            );
        }

        $action = new AddThreadSave($this->context->getService("forumThreadRepository"), $this->context->getService("forumTopicRepository"), $user);
        $form->onSuccess[] = array($action, "execute");
        $form->onSuccess[] = array($this, "addTopicSuccess");

        return $form;
    }

    public function addTopicSuccess(Form $form)
    {
        $this->flashMessage("Topic úspěšně přidán.");
        $this->redirect("Forum:");
    }
}