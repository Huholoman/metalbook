<?php

namespace App\Presenters;

use App\Components\Forms\Sign\actions\RegisterAction;
use Nette,
    App\Model;


/**
 * Sign in/out presenters.
 */
class SignPresenter extends BasePresenter
{
    public function renderDefault()
    {
        $this->redirect("Sign:in");
    }

    public function createComponentRegisterForm($name)
    {
        $form = new \App\Components\Forms\Sign\RegisterForm($this, $name);

        $action = new RegisterAction($this->context->getService("userRepository"));
        $form->onSuccess[] = callback($action, "execute");
        $form->onSuccess[] = callback($this, "handleRegisterSuccess");
        $form->onError[] = callback($this, "handleError");

        return $form;
    }

    public function handleRegisterSuccess(\Nette\Forms\Form $form)
    {
        $this->flashMessage("Registrace proběhla úspěšně.", "success");
    }


    public function beforeSign()
    {
        if ($this->getUser()->isLoggedIn()) {
            $this->redirect("User:default");
        }
    }

    public function renderLogout()
    {
        $this->getUser()->logout(true);
        $this->flashMessage("Odhlášení proběhlo úspěšně.", "success");
        $this->redirect("Home:default");
    }

    public function createComponentLoginForm($name)
    {
        $form = new \App\Components\Forms\Sign\LoginForm($this, $name);


        $user = $this->getUser();
        $action = new \App\Components\Forms\actions\LoginFormAction($user);
        $form->onSuccess[] = callback($action, "execute");
        $form->onSuccess[] = callback($this, "handleLoginSuccess");
        $form->onError[] = callback($this, "handleError");

        return $form;
    }

    public function handleError(\Nette\Forms\Form $form)
    {
        $this->flashMessage("Neplatný uživatelský login nebo heslo.", "error");
    }

    public function handleLoginSuccess(\Nette\Forms\Form $form)
    {
        $this->flashMessage("Přihlášení proběhlo úspěšně.", "success");
        $this->redirect("Home:default");
    }

}
