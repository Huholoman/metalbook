<?php

namespace App\Presenters;

use Nette\Application\UI\Form;

class UserPresenter extends SecurePresenter
{

    public function createComponentDataChangeForm($name)
    {
        $user = $this->getUser();
        $userRepository = $this->context->UserRepository;

        $form = new \App\Components\Forms\User\ChangeDataForm($this, $name);

        $user = $userRepository->find($user->getId());
        $form->setDefaults(
            array(
                "age" => $user->age,
                "description" => $user->description,
                "musicDescription" => $user->description
            )
        );

        $action = new \App\Components\Forms\actions\ChangePDataAction($userRepository, $user);
        $form->onSuccess[] = callback($action, "execute");
        $form->onSuccess[] = callback($this, "handleDataChangedSuccess");
        return $form;
    }

    public function createComponentPassChangeForm($name)
    {
        $user = $this->getUser();
        $userRepository = $this->context->UserRepository;

        $form = new \App\Components\Forms\User\ChangePassForm($this, $name);

        $action = new \App\Components\Forms\actions\ChangePassAction($userRepository, $user);
        $form->onSuccess[] = callback($action, "execute");
        $form->onSuccess[] = callback($this, "handlePassChangedSuccess");

        return $form;
    }

    public function createComponentEmailChangeForm($name)
    {
        $form = new \App\Components\Forms\User\ChangeEmailForm($this, $name);

//        $actionCE = new \components\Forms\actions\ChangeEmailAction($model, $user);
//        $formCE->onSuccess[] = callback($actionCE, "execute");
        $form->onSuccess[] = callback($this, "handleEmailChangedSuccess");

        return $form;
    }

    public function handleDataChangedSuccess()
    {
        $this->flashMessage("Data změněna.", "Ok");
    }

    public function handlePassChangedSuccess()
    {
        $this->flashMessage("Heslo změněno.", "Ok");
    }

    public function handleEmailChangedSuccess()
    {
        $this->flashMessage("Email změněn.", "Ok");
    }

}